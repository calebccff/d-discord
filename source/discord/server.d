module discord.server;

import std.json;
import std.stdio;

struct Server
{
    @SerializedName("afk_channel_id");
    string afkChannelID;
    @SerializedName("afk_timeout");
    int afkTimeout = 0;
    @SerializedName("application_id");
    // Application ID
    string applicationID;

    @SerializedName("channels");
    JSONValue channels;
}