/**
 * d-discord
 * A simple, easy to use Discord library for making bots written in 
 * the D programming language.
 * 
 * Author: Dhillon (dhilln)
 * License: See LICENSE for details.
**/

module discord.endpoints;

enum Endpoints : string
{
    gateway = "https://discordapp.com/api/gateway/bot",
    websocketGateway = "https://gateway.discord.gg"
} 